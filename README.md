# 前端工具库


## JS工具库

### 综合工具库
* [Licia](https://github.com/liriliri/licia)是一套在开发中实践积累起来的实用 JavaScript 工具库
* [mathjs](https://github.com/josdejong/mathjs) 函数库
* [lodash](https://github.com/lodash/lodash)

### String
* [voca](https://github.com/panzerdp/voca)  - a JavaScript library for manipulating strings

### Number
*数字工具库*

* [Complex.js](https://github.com/infusion/Complex.js) - A complex number library for JavaScript.
* [Fraction.js](https://github.com/infusion/Fraction.js) 

### Date
* [moment](https://github.com/moment/moment) A lightweight JavaScript date library for parsing, validating, manipulating, and formatting dates.
* [dayjs](https://github.com/iamkun/dayjs)  是一个轻量的处理时间和日期的 JavaScript 库，和 Moment.js 的 API 设计保持完全一样. 如果您曾经用过 Moment.js, 那么您已经知道如何使用 Day.js
* [Datejs](https://github.com/datejs/Datejs)


## 拖拽和排序

* [Sortable](https://github.com/SortableJS/Sortable) - 可以集成vue react jquery 
* [gridstack.js](https://github.com/gridstack/gridstack.js) - Making a drag-and-drop, multi-column responsive dashboard has never been easier.
* [packery](https://github.com/metafizzy/packery)

## API
* [axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js


## 页面加载

* [Spin.js](https://github.com/fgnass/spin.js) 
* [nanobar](https://github.com/jacoborus/nanobar)
* [Ladda](https://github.com/hakimel/Ladda)
* [css-loaders](https://github.com/lukehaas/css-loaders)
* [pace](https://github.com/HubSpot/pace) 各种加载样式

## 表单验证
* [jquery-validation](https://github.com/jquery-validation/jquery-validation)
* [validator.js](https://github.com/validatorjs/validator.js) 针对字符串的验证和工具
* [is.js](https://github.com/arasatasaygin/is.js) js对象检查库
* [Parsley.js ](https://github.com/guillaumepotier/Parsley.js) html元素上填写验证规则的验证库
* [joi](https://github.com/sideway/joi)

## 键盘事件库
* [mousetrap](https://github.com/ccampbell/mousetrap) 是一个超简单的处理键盘快捷键的类库，压缩有只有1.6kb大小，并且不依赖任何的外部类库。
* [hotkeys](https://github.com/jaywcjlove/hotkeys)

## Tours And Guides
* [intro.js](https://github.com/usablica/intro.js) A better way for new feature introduction and step-by-step users guide for your website and project.
* [shepherd](https://github.com/shipshapecode/shepherd) Guide your users through a tour of your app.
* [bootstrap-tour](https://github.com/sorich87/bootstrap-tour) Quick and easy product tours with Twitter Bootstrap Popovers
* [Chardin.js](https://github.com/heelhook/chardin.js) 可以帮助我们实现别致的遮罩介绍效果。它使用给定的元素的自定义数据属性“中的信息，一旦触发便会激活用于指引和介绍的遮罩效果。
* [Driver.js](https://github.com/kamranahmedse/driver.js) 突出显示页面上的任何任何项目,锁定用户交互,创建功能介绍

## Notifications
* [toastr](https://github.com/CodeSeven/toastr) Javascript library for non-blocking notifications. jQuery is required. The goal is to create a simple core library that can be customized and extended.
* [notie](https://github.com/jaredreich/notie) 一款简单实用的纯JavaScript消息提示插件。
* [sweetalert](https://github.com/t4t5/sweetalert) A beautiful replacement for JavaScript's "alert"
* [sweetalert2](https://github.com/sweetalert2/sweetalert2) A beautiful, responsive, customizable, accessible (WAI-ARIA) replacement for JavaScript's popup boxes. Zero dependencies.
* [pnotify](https://github.com/sciactive/pnotify)
  

## Sliders
* [Swiper](https://github.com/nolimits4web/Swiper) 触摸滑动插件
* [slick](https://github.com/kenwheeler/slick)
* [reveal.js](https://github.com/hakimel/reveal.js) 基于CSS的3D幻灯片工具
* [PhotoSwipe](https://github.com/dimsemenov/PhotoSwipe) 图片查看插件
  

## 表单控件


### input自动补全

* [typeahead.js](https://github.com/twitter/typeahead.js) 自动补全插件
* [jQuery-Autocomplete](https://github.com/devbridge/jQuery-Autocomplete)

### input格式输入

* [Inputmask](https://github.com/RobinHerbots/Inputmask) 输入数据进行格式化展示
* [jQuery-Mask-Plugin](https://github.com/igorescobar/jQuery-Mask-Plugin) 输入数据进行格式化展示

### 日期控件

* [bootstrap-datepicker](https://github.com/uxsolutions/bootstrap-datepicker)
* [fullcalendar](https://github.com/fullcalendar/fullcalendar)
* [bootstrap-datetimepicker](https://github.com/Eonasdan/bootstrap-datetimepicker)
* [daterangepicker](https://github.com/dangrossman/daterangepicker)
* [flatpickr](https://github.com/flatpickr/flatpickr)

### Select
* [selectize.js](https://github.com/selectize/selectize.js)
* [Select2](https://github.com/select2/select2)

### File Uploader 

* [jQuery-File-Upload](https://github.com/blueimp/jQuery-File-Upload)
* [dropzone](https://github.com/enyo/dropzone)
* [bootstrap-fileinput](https://github.com/kartik-v/bootstrap-fileinput)

### Check
* [icheck](https://github.com/fronteed/icheck)

### toggle
* [bootstrap-toggle](https://github.com/minhur/bootstrap-toggle)

### wizard
* [jquery-smartwizard](https://github.com/techlab/jquery-smartwizard)

### 富文本
1. [summernote](https://github.com/summernote/summernote)
2. [ueditor](https://github.com/fex-team/ueditor)
3. [neditor](https://github.com/notadd/neditor)
4. 




## Scroll
* [fullPage](https://github.com/alvarotrigo/fullPage.js) 全屏滚动

## 菜单
* [jQuery-contextMenu](https://github.com/swisnl/jQuery-contextMenu) 弹出菜单
* [metismenu](https://github.com/onokumus/metismenu)

## 表格

* [DataTables](https://www.datatables.net/)
* [bootstrap-table](https://bootstrap-table.com)
* [vxe-table](https://gitee.com/xuliangzhan_admin/vxe-table)

## Timeline
* [TimelineJS3](https://github.com/NUKnightLab/TimelineJS3)
* [timesheet.js](https://github.com/sbstjn/timesheet.js)

## Tab
* [jQuery-EasyTabs](https://github.com/JangoSteve/jQuery-EasyTabs)

## 树控件
1. [zTree_v3](https://github.com/zTree/zTree_v3)
2. [jstree](https://github.com/vakata/jstree)


## 动画库
1. [Hover.css](https://github.com/IanLunn/Hover) 链接鼠标停留动态样式
2. [animate.css](https://github.com/animate-css/animate.css)


## 画图工具
1. [mxgraph](https://github.com/jgraph/mxgraph) 画流程图工具
2. [js-sequence-diagrams](https://github.com/bramp/js-sequence-diagrams) 简单文本生成UML图
3. [Snap.svg](https://github.com/adobe-webplatform/Snap.svg)
4. [flowchart.js](https://github.com/adrai/flowchart.js)
5. [raphael](https://github.com/DmitryBaranovskiy/raphael)
6. [GraphVis](https://gitee.com/baopengdu/GraphVis) 可以应用于知识图谱可视化，复杂网络可视化分析，关系图可视化，网络拓扑图，布局算法，社区发现算法等应用场景。也可以作为 network,graph,knowlegegraph，neo4j，gephi相关应用工具的核心算法层。

## 图表库
1. [Chart.js](https://github.com/chartjs/Chart.js)
2. [ECharts](https://github.com/apache/incubator-echarts)
   

## 前端模板
1. [AdminLTE](https://github.com/ColorlibHQ/AdminLTE)
2. [gentelella](https://github.com/ColorlibHQ/gentelella)
3. [renren-fast-vue](https://github.com/renrenio/renren-fast-vue)
4. [vue-element-admin](https://gitee.com/panjiachen/vue-element-admin)  是一个后台前端解决方案，它基于 vue 和 element-ui实现。它使用了最新的前端技术栈，内置了 i18n 国际化解决方案，动态路由，权限验证，提炼了典型的业务模型，提供了丰富的功能组件，它可以帮助你快速搭建企业级中后台产品原型
5. [iview-admin](https://github.com/iview/iview-admin)
6. [layuimini](https://gitee.com/baopengdu/layuimini) 后台admin前端模板，基于 layui 编写的最简洁、易用的后台框架模板。只需提供一个接口就直接初始化整个框架，无需复杂操作。


## 组件库
1. [muse-ui](https://github.com/museui/muse-ui)
2. [touchui](https://github.com/uileader/touchui)
3. [iview](https://github.com/iview/iview)
4. [layui](https://github.com/sentsin/layui)
5. [Semantic-UI](https://github.com/Semantic-Org/Semantic-UI)
6. [ak-vue](https://gitee.com/q337547038/ak-vue) 基于Vue2.x的一套PC端UI组件库，包含了 AutoForm 自动表单、BackTop 返回顶部、Breadcrumb 面包屑、 Button 按钮、Cascader 级联选择器、Checkbox 多选框、Collapse 折叠面板、ColorPicker 颜色选择器、DataPicker 时间选择器、Dialog 弹层对话框、Alert 弹框、Echarts 图形图表、Form 表单、Input 输入框、Lazy 图片延时加载、Loading 加载等待、Menu 菜单、Pagination 分页、Progress 进度条、Radio 单选框、Select 选择器、Steps 步骤条、Swiper 跑马灯图片轮播、Switch 开关、Table 表格、Tabs 标签页、Textarea 文本框、Tooltip 提示、Transfer 穿梭框、Tree 树形、Upload 上传等组件
7. [Avue](https://gitee.com/smallweigit/avue) 是基于现有的element-ui库进行的二次封装，简化一些繁琐的操作，核心理念为数据驱动视图,主要的组件库针对table表格和form表单场景，同时衍生出更多企业常用的组件，达到高复用，容易维护和扩展的框架，同时内置了丰富了数据展示组件，让开发变得更加容易，avue-cli是后台模版
8. [ant-design-vue](https://github.com/vueComponent/ant-design-vue)




# 后端库

## 企业快速开发平台

1. [jeesite4](https://github.com/thinkgem/jeesite4) 基于 Spring Boot、Spring MVC、Shiro、MyBatis、Beetl、Bootstrap、AdminLTE 在线代码生成功能，采用经典开发模式，让初学者能够更快的入门并投入到团队开发中去。在线代码生成功能，包括模块如：组织机构、角色用户、菜单及按钮授权、数据权限、系统参数、内容管理、工作流等。采用松耦合设计，模块增减便捷；界面无刷新，一键换肤；众多账号安全设置，密码策略；文件在线预览；消息推送；多元化第三方登录；在线定时任务配置；支持集群，支持SAAS；支持多数据源；支持读写分离、分库分表；支持微服务应用
2. [SpringBlade](https://github.com/chillzhuang/SpringBlade)
3. [paascloud-master](https://github.com/paascloud/paascloud-master)
4. [piggymetrics](https://github.com/sqshq/piggymetrics)
5. [jeesite](https://github.com/thinkgem/jeesite)
6. [RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue) 基于SpringBoot，Spring Security，JWT，Vue & Element 的前后端分离权限管理系统
7. [KendoUI-Admin-Site](https://gitee.com/IKKI2000/KendoUI-Admin-Site) 是基于 Kendo UI for jQuery 和 Bootstrap 4 搭建的前台网站和后台管理框架。
8. [NutzFw](https://gitee.com/threefish/NutzFw) Java开源企业级快速开发框架、后台管理系统，拥有完善的权限控制、代码生成器、自定义表单、动态数据库、灵活的工作流、手机APP客户端、支持前后端分离开发。
9. [Pro-Cloud](https://gitee.com/gitsc/pro-cloud) 是一个SpringCloud alibaba 2.2.1 微服务架构springboot2.2.5+mybatisplus基于oauth2.0认证,采用Nacos注册和配置中心，集成流量卫兵Sentinel的分布式架构。解决常见的分布式问题, redis/zookeeper分布式锁,分布式事务,灰度发布,sso单点登录log4j2日志,多租户问题等
10. [pig](https://gitee.com/log4j/pig) 基于Spring Boot 2.3、 Spring Cloud Hoxton & Alibaba、 OAuth2 的RBAC 权限管理系统。
11. [zuihou-admin-cloud](https://gitee.com/zuihou111/zuihou-admin-cloud) 基于SpringCloud(Hoxton.SR3) + SpringBoot(2.2.7.RELEASE) 的SaaS 微服务脚手架，具有统一授权、认证后台管理系统，其中包含具备用户管理、资源权限管理、网关API、分布式事务、大文件断点分片续传等多个模块，支持多业务系统并行开发，可以作为后端服务的开发脚手架。代码简洁，架构清晰，适合学习和直接项目中使用。核心技术采用Nacos、Fegin、Ribbon、Zuul、Hystrix、JWT Token、Mybatis、SpringBoot、Redis、RibbitMQ等主要框架和中间件
12. [Cloud-Platform](https://gitee.com/geek_qi/cloud-platform) 是国内首个基于Spring Cloud微服务化开发平台，具有统一授权、认证后台管理系统，其中包含具备用户管理、资源权限管理、网关API 管理等多个模块，支持多业务系统并行开发，可以作为后端服务的开发脚手架。代码简洁，架构清晰，适合学习和直接项目中使用。 核心技术采用Spring Boot 2.1.2以及Spring Cloud (Greenwich.RELEASE) 相关核心组件，采用Nacos注册和配置中心，集成流量卫兵Sentinel，前端采用vue-element-admin组件。
13. [eladmin](https://gitee.com/elunez/eladmin) 项目基于 Spring Boot 2.1.0 、 Jpa、 Spring Security、redis、Vue的前后端分离的后台管理系统，项目采用分模块开发方式， 权限控制采用 RBAC，支持数据字典与数据权限管理，支持一键生成前后端代码，支持动态路由
14. [Guns](https://gitee.com/stylefeng/guns) 基于Spring Boot2，致力于做更简洁的后台管理系统。包含系统管理，代码生成，多数据库适配，SSO单点登录，工作流，短信，邮件发送，OAuth2登录，任务调度，持续集成，docker部署等功。支持Spring Cloud Alibaba微服务



## 分布式事务
1. [tx-lcn](https://github.com/codingapi/tx-lcn)
2. [seata](https://github.com/seata/seata)

## 分布式日志系统
1. [Plumelog](https://gitee.com/frankchenlong/plumelog) 一个java分布式日志系统，支持百亿级别，日志从搜集到查询，不用去文件中翻阅日志方便快捷，支持查询一个调用链的日志，支持链路追踪，查看调用链耗时情况，在分布式系统中也可以查询关联日志，能够帮助快速定位问题，简单易用，没有代码入侵，查询界面友好，高效，方便，只要你是java系统，不要做任何项目改造，接入直接使用，日志不落本地磁盘，无需关心日志占用应用服务器磁盘问题,觉得项目好用帮忙点个星星，您的star是我们前进的动力

## 中文分词
1. [HanLP](https://github.com/hankcs/HanLP)

## 流程引擎
1. [uflo](https://github.com/youseries/uflo) 是一款基于Spring的纯Java流程引擎，支持并行、动态并行、串行、会签等各种流转方式。
2. [flowable-engine](https://github.com/flowable/flowable-engine)
3. [Activiti](https://github.com/Activiti/Activiti)
4. [JFlow](https://gitee.com/opencc/JFlow) 驰骋BPM系统包含表单引擎+流程引擎+权限控制,方便集成,配置灵活,功能强大,适合中国国情的工作流引擎
5. [agile-bpm-basic](https://gitee.com/agile-bpm/agile-bpm-basic) 企业级流程解决方案， 前后端分离，模块化，超低耦合。 基于activiti5.22，零java代码即可做到复杂业务的流程实施


## 工具库
1. [hutool](https://gitee.com/loolly/hutool) 是一个小而全的Java工具类库，使Java拥有函数式语言般的优雅，让Java语言也可以“甜甜的”。
2. [pacebox-jwt](https://gitee.com/pacebox/pacebox-jwt) 是一个基于jwt和pacebox-core封装的便捷工具包、简单几行代码即可对jwt进行操作
3. [Happy-Captcha](https://gitee.com/ramostear/Happy-Captcha) 是一款易于使用的Java验证码软件包，旨在花最短的时间，最少的代码量，实现Web站点的验证码功能。


## SSO解决方案
1. [MaxKey](https://gitee.com/maxkeytop/MaxKey) (马克思的钥匙)单点登录认证系统(Sigle Sign On System),寓意是最大钥匙,支持OAuth2.0/OpenID Connect、SAML2.0、JWT、CAS、SCIM等标准协议

## 办公文档工具
1. [x-easypdf](https://gitee.com/xsxgit/x-easypdf) 只需一行代码搞定pdf的框架
2. [easypoi](https://gitee.com/lemur/easypoi) POI 工具类,Excel的快速导入导出,Excel模板导出,Word模板导出,可以仅仅5行代码就可以完成Excel的导入导出,修改导出格式简单粗暴,快速有效
3. [kkFileView](https://gitee.com/kekingcn/file-online-preview) 使用spring boot打造文件文档在线预览项目解决方案，支持doc、docx、ppt、pptx、xls、xlsx、zip、rar、mp4、mp3以及众多类文本如txt、html、xml、java、properties、sql、js、md、json、conf、ini、vue、php、py、bat、gitignore等文件在线预览
4. [easyexcel](https://github.com/alibaba/easyexcel) 重写了poi对07版Excel的解析，能够原本一个3M的excel用POI sax依然需要100M左右内存降低到几M，并且再大的excel不会出现内存溢出，03版依赖POI的sax模式。在上层做了模型转换的封装，让使用者更加简单方便


## 缓存
1. [J2Cache](https://gitee.com/ld/J2Cache) java 两级缓存框架，可以让应用支持两级缓存框架 ehcache(Caffeine) + redis 。避免完全使用独立缓存系统所带来的网络IO开销问题

## 数据库连接池
1.[HikariCP](https://github.com/brettwooldridge/HikariCP)

## 分布式调度平台
1. [xxl-jobJ](https://gitee.com/xuxueli0323/xxl-job) 是一个轻量级分布式任务调度框架，支持通过 Web 页面对任务进行 CRUD 操作，支持动态修改任务状态、暂停/恢复任务，以及终止运行中任务，支持在线配置调度任务入参和在线查看调度结果。
2. [Quartz]() 分布式集群开源工具，以下两个分布式任务应该都是基于Quartz实现的，可以说是中小型公司必选
3. [Elastic-Job]() 是一个分布式调度解决方案，由两个相互独立的子项目 Elastic-Job-Lite 和 Elastic-Job-Cloud 组成。定位为轻量级无中心化解决方案，使用 jar 包的形式提供分布式任务的协调服务。支持分布式调度协调、弹性扩容缩容、失效转移、错过执行作业重触发、并行调度、自诊.
4. [Saturn]() 是唯品会在github开源的一款分布式任务调度产品。它是基于当当elastic-job来开发的，其上完善了一些功能和添加了一些新的feature。目前在github上开源大半年，470个star。Saturn的任务可以用多种语言开发比如python、Go、Shell、Java、Php。其在唯品会内部已经发部署350+个节点，每天任务调度4000多万次。同时，管理和统计也是它的亮点。

## 高并发框架
1. [Disruptor](https://github.com/LMAX-Exchange/disruptor) 是一个高性能的异步处理框架,一个“生产者-消费者”模型
2. [JCTools](https://github.com/JCTools/JCTools)  提供了一系列非阻塞并发数据结构（标准 Java 中缺失的），当存在线程争抢的时候，非阻塞并发数据结构比阻塞并发数据结构能提供更好的性能。
3. [asyncTool](https://gitee.com/jd-platform-opensource/asyncTool) 解决任意的多线程并行、串行、阻塞、依赖、回调的并行框架，可以任意组合各线程的执行顺序，带全链路执行结果回调。多线程编排一站式解决方案。来自于京东主App后台。


## JSON
1. [Gson](https://github.com/google/gson)
2. [Jackson](https://github.com/FasterXML/jackson)
3. [fastjson](https://github.com/alibaba/fastjson)


## 验证码
1. [EasyCaptcha](https://github.com/whvcse/EasyCaptcha)  Java图形验证码，支持gif、中文、算术等类型，可用于Java Web、JavaSE等项目。
2. [AJ-Captcha](https://gitee.com/anji-plus/captcha) 行为验证码(滑动拼图、点选文字)，前后端(java)交互，包含vue/h5/Android/IOS/flutter/uni-app的源码和实现


## 支付
1. [spring-boot-pay](https://gitee.com/52itstyle/spring-boot-pay) 支付服务：支付宝，微信，银联详细代码案例；支付API文档、持续更新中
2. [IJPay](https://gitee.com/javen205/IJPay) 聚合支付，IJPay 让支付触手可及，封装了微信支付、QQ支付、支付宝支付、京东支付、银联支付、PayPal支付等常用的支付方式以及各种常用的接口。不依赖任何第三方 mvc 框架，仅仅作为工具使
3. [xxpay](https://gitee.com/jmdhappy/xxpay-master) 聚合支付使用Java开发，包括spring-cloud、dubbo、spring-boot三个架构版本，已接入微信、支付宝等主流支付渠道，可直接用于生产环境
4. [pay-java-parent](https://gitee.com/egzosn/pay-java-parent) 全能第三方支付对接Java开发工具包.优雅的轻量级支付模块集成支付对接支付整合（微信,支付宝,银联,友店,富友,跨境支付paypal,payoneer(P卡派安盈)易极付）app,扫码,网页支付刷卡付条码付刷脸付




# 其他

## 文件同步
* [syncthing](https://github.com/syncthing/syncthing)


## 私有网盘系统
* [kiftd](https://github.com/KOHGYLW/kiftd) kiftd是一款便捷、开源、功能完善的 JAVA 网盘 / 云盘 系统。专门面向个人、团队或小型组织来搭建属于自己的网盘。它不仅仅是替代U盘的不二之选，还是一款具备在线视频播放、文档在线预览、音乐播放、图片查看等高级功能的文件云存储平台。仅需3分钟，您就能在任意平台上轻松安装然后开始使用它

## IM聊天软件
1. [V-IM](https://gitee.com/lele-666/V-IM) 基于JS的超轻量级聊天软件。前端：vue、iview、electron实现的 PC 桌面版聊天程序，主要适用于私有云项目内部聊天，企业内部管理通讯等功能，主要通讯协议websocket。支持web网页聊天实现。服务端： springboot、tio、oauth2.0等技术
2. [oim-fx](https://gitee.com/oimchat/oim-fx) 即时通讯的聊天系统,服务端和客户端整个完整体系,服务端目前支持Socket、WebSocket,客户端是JavaFX开发
3. [oim-e](https://gitee.com/oimchat/oim-e) 是基于Electron实现的PC桌面聊天软件，可支持Windows、Linux、Mac等主流平台。 主要采用了Vue、iView、TypeScript等技术实现，通讯协议主要是websocket。
4. [oim-web](https://gitee.com/oimchat/oim-web) 这是静态页面项目，直接双击index.html浏览器打开就OK了 OIM即时通讯的网页版客户端


## 项目管理
* [masterlab](https://github.com/gopeak/masterlab)

## docker 镜像管理系统
* [harbor](https://github.com/goharbor/harbor)

## docker 容器管理消停
* [portainer](https://github.com/portainer/portainer)


#.net

## 综合工具类
1. [DotNetCodes](https://gitee.com/kuiyu/dotnetcodes) 它是一个类库，里面包含大量可直接使用的功能代码，可以帮你减少开发与调试时间,而且类与类之间没有什么依赖,每个类都可以单独拿出来使用。

## winform 控件库
1. [HZHControls控件库](https://gitee.com/kwwwvagaa/net_winform_custom_control)  c#的winform自定义控件，对触屏具有更好的操作支持，项目是基于framework4.0，完全原生控件开发，没有使用任何第三方控件，你可以放心的用在你的项目中(winfromcontrol/winformcontrol/.net)。还有更丰富的工业控件持续增加中
2. [SunnyUI](https://gitee.com/yhuse/SunnyUI) 基于 C# .Net WinForm 开源控件库、工具类库、扩展类库、多页面开发框架
